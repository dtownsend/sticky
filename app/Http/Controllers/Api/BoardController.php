<?php

namespace App\Http\Controllers\Api;

use App\Board;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BoardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except('show');
    }

    // List all boards
    public function index(Request $request)
    {
        return ['boards' => $request->user()->boards()->get()];
    }

    // Load a board
    public function show(Request $request, Board $board)
    {
        if ($board->public !== true) {
            $board = $request->user()->boards()->findOrFail($board->id);
        }

        return ['board' => $board];
    }

    // Save a board
    public function store(Request $request)
    {
        $data = json_decode($request->input('json'));

        return $request->user()->boards()->create([
            'title' => $data->title,
            'json' => json_encode($data->board),
        ]);
    }

    public function update(Request $request, Board $board)
    {
        $data = json_decode($request->input('json'));

        $board = $request->user()->boards()->findOrFail($board->id);

        $board->title = $data->title;
        $board->public = $data->public;
        $board->json = json_encode($data->board);
        $board->save();

        return $board;
    }

}
