<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Board extends Model
{
    protected $fillable = ['title', 'json', 'public'];

    protected $appends = ['draggables', 'connections'];

    protected $casts = [
        'public' => 'boolean',
    ];

    public function getDraggablesAttribute()
    {
        $json = json_decode($this->json);

        if (property_exists($json, 'draggables')) {
            return $json->draggables;
        }
    }

    public function getConnectionsAttribute()
    {
        $json = json_decode($this->json);

        if (property_exists($json, 'connections')) {
            return $json->connections;
        }
    }
}
