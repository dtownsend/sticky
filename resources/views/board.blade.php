<html>
    <head>
        <title>Sticky</title>
        <script
                src="https://code.jquery.com/jquery-3.4.1.min.js"
                integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
                crossorigin="anonymous"></script>
        <script
                src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
                integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
                crossorigin="anonymous"></script>
        <script src="js/TouchPunchMonkeyPatch.js"></script>
        <script src="js/connections.js"></script>
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js" integrity="sha384-xrRywqdh3PHs8keKZN+8zzc5TX0GRTLCcmivcbNJWm2rs5C8PRhcEn3czEjhAO9o" crossorigin="anonymous"></script>

        <script>
            let auth = {{ auth()->check()? 'true': 'false' }};
        </script>
        <script type="module" src="js/app.js"></script>

        <style>
            body {
                background-color: #a1cbef;
            }

            nav {
                padding:5px 0;
            }

            #canvas {
                background-color:white;
                position:absolute;
                width:2500px;
                height:1500px;
                margin:0 auto;
            }

            /*nav {*/
            /*    width*/
            /*}*/

            .draggable {
                z-index: 100;
                cursor: grab;
            }

            .ui-draggable-dragging {
                box-shadow: 3px 3px 5px 6px #ccc;
            }

            .draggable-tool {
                position:absolute;
                top:-30px;
            }

            .sticky {
                outline:none;
                width:100%;
                height:100%;
                font-size: 21px;
                /*overflow:hidden;*/
                white-space: pre-wrap;
                text-align: center;
                display: inline-flex;
                align-items: center;
                justify-content: center;
            }

            .selected {
                border: 2px dashed #7594ff;
                z-index: 900;
            }

            .selected .sticky {
                cursor: pointer;
            }

            .editing .sticky {
                cursor: text;
            }

            .editing {
                border: 2px solid #7594ff;
            }

            .selection {
                border: 1px solid #7594ff;
            }

            .connection {
                color: #00b8d4;
            }

            .connection.active {
                color: #FD6068;
            }
        </style>
    </head>
    <body>
        <div id="board" class="container-fluid">
            @guest
                @if (request()->get('board'))
                    <p class="alert alert-primary text-center" id="view-only">You are not logged in. You may view the board, but no changes will be saved.</p>
                @else
                    <p class="alert alert-primary text-center">Welcome to Sticky. Have a play. If you think it could be useful then create an account so you can store your boards.</p>
                @endif
            @endguest

            <p class="alert alert-danger" id="failed-to-load" style="display:none">You do not have access to view this board.</p>
            <p class="alert alert-danger" id="failed-to-save" style="display:none">Attempt to save failed.</p>

            <nav class="row">
                @auth()
                    <div class="col-4 mt-2">
                        <select name="boards"></select>
                        <button id="load">Load</button>
                        <input type="text" name="title" value="">
                        <label for="public" title="Anyone can see (but not edit) this board">Public</label>
                        <input type="checkbox" id="public" name="public" value="true">
                        <button id="save">Save</button>
                        <button id="new">New</button>
                    </div>
                @endauth

                <div id="create-stickies" class="col-6"></div>

                @auth
                    <div class="col-2 mt-2">
                        <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" style=" float:right;">
                            {{ __('Logout') }}
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                @else
                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                @endauth
            </nav>

            <div id="canvas"></div>
        </div>
    </body>
</html>
