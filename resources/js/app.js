import Sticky from './Sticky.js';
import Board from './Board.js';

let colours = [
    'fcfe7d',
    'ffffff',
    '4DCAC0',
    '808A89',
    '34CC25',
    'FFA661',
    'FD6068'
];

let sizes = [
    {x: 120, y: 120},
    {x: 240, y: 120},
    {x: 240, y: 240},
];

$(document).ready(function() {
    sizes.forEach(function (size) {
        colours.forEach(function (colour) {
            let ele = $('<button class="create-sticky" style="background-color:'+colour+';width:'+(size.x / 5)+'px;height:'+(size.y / 5)+'px;">&nbsp</button>');

            $('#create-stickies').append(ele);

            ele.click(function (e) {
                new Sticky(10, 10, size.x, size.y, 'Drag me!', colour).putInDom();
            });
        });
    });

    // List all boards
    Board.list();

    $('#load').click(function() {
        Board.load();
    });

    $('#save').click(function() {
        Board.save();
    });

    Board.canvas.on('paste', function(e) {
        // Use our own paste method to force plain text
        e.preventDefault();

        let text = '';
        if (e.clipboardData || e.originalEvent.clipboardData) {
            text = (e.originalEvent || e).clipboardData.getData('text/plain');
        } else if (window.clipboardData) {
            text = window.clipboardData.getData('Text');
        }
        if (document.queryCommandSupported('insertText')) {
            document.execCommand('insertText', false, text);
        } else {
            document.execCommand('paste', false, text);
        }
    });

    Board.canvas.mousedown(function(e) {
        Board.startSelection(e);
    });

    Board.canvas.mouseup(function(e) {
        Board.endSelection(e);
    });
});
