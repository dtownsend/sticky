## New clone
Let's get docker up and running
```
$ git submodule init
$ git submodule update
$ cd laradock-sticky
$ cp .env.example .env 
$ docker-compose up -d --build nginx mysql
// Wait for several minutes
$ docker down
$ docker-compose up -d nginx mysql
$ docker-compose exec workspace bash
$ cp .env.example .env
$ composer install
$ art key:generate
$ art migrate
```
http://localhost should now by service the app

## Useful docker stuff
```
$ docker-compose up -d nginx mysql
$ docker-compose exec workspace bash
$ docker-compose exec nginx bash
$ docker-compose exec mysql bash
$ docker-compose down
```

## Deploying to ecs
Install aws cli first - https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-install.html
And create an IAM user with AdminstratorAccess 
```
$ aws configure
$ (aws ecr get-login --no-include-email --region eu-west-1)
```
Copy and paste the output of that as a new command (may need to edit in a scratch file to remove new lines)
```
$ docker tag sticky-docker_workspace 292099829357.dkr.ecr.eu-west-1.amazonaws.com/sticky/workspace:latest
$ docker push 292099829357.dkr.ecr.eu-west-1.amazonaws.com/sticky/workspace:latest
```
