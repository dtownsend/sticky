import Sticky from './Sticky.js';
import Board from './Board.js';
import AreaSelect from "./AreaSelect.js";

let colours = [
    'fcfe7d',
    'ffffff',
    '4DCAC0',
    '808A89',
    '34CC25',
    'FFA661',
    'FD6068'
];

let sizes = [
    {x: 120, y: 120},
    {x: 240, y: 120},
    {x: 240, y: 240},
];

$(document).ready(function() {
    // Load a board if specified in the url
    let boardId = new URLSearchParams(window.location.search).get('board');
    if (boardId) {
        Board.load(boardId);
    }

    sizes.forEach(function (size) {
        colours.forEach(function (colour) {
            let ele = $('<button class="create-sticky" style="background-color:'+colour+';width:'+(size.x / 5)+'px;height:'+(size.y / 5)+'px;">&nbsp</button>');

            $('#create-stickies').append(ele);

            ele.click(function (e) {
                new Sticky(10, 10, size.x, size.y, 'Drag me!', colour).putInDom();
            });
        });
    });

    if (auth !== true) {
        return;
    }

    // List all boards
    Board.list();

    $('#load').click(function() {
        // Update url
        let url = new URL(window.location.origin);
        url.searchParams.set('board', Board.loadSelect.val());
        window.location.href = url.href;
    });

    $('#save').click(function() {
        Board.save();
    });

    $('#new').click(function() {
        let url = new URL(window.location.origin);
        url.searchParams.set('board', '');
        window.location.href = url.href;
    });

    // Save every few seconds
    let saveInterval = setInterval(function() {Board.save()}, 5000);

    Board.canvas.on('paste', function(e) {
        // Use our own paste method to force plain text
        e.preventDefault();

        let text = '';
        if (e.clipboardData || e.originalEvent.clipboardData) {
            text = (e.originalEvent || e).clipboardData.getData('text/plain');
        } else if (window.clipboardData) {
            text = window.clipboardData.getData('Text');
        }
        if (document.queryCommandSupported('insertText')) {
            document.execCommand('insertText', false, text);
        } else {
            document.execCommand('paste', false, text);
        }
    });

    Board.canvas.mousedown(function(e) {
        clearTimeout(Board.longPressTimeout);
        if ($(e.target).hasClass('selection') || $(e.target).parents('.selection').length) {
            // a click within a selection should never create a new selection
            return;
        }

        if (Board.selection) {
            Board.selection.destroy();
        }

        if ($(e.target)[0] !== $('#canvas')[0]) {
            return false;
        }

        Board.longPressTimeout = window.setTimeout(function() {
            Board.selection = new AreaSelect(e.clientX, e.clientY);
            Board.selection.putInDom();
        },200);
    });

    Board.canvas.mouseup(function(e) {
        clearTimeout(Board.longPressTimeout);

        if (Board.selection) {
            if ($(e.target)[0] !== $('#canvas')[0]) {
                return false;
            }

            Board.selection.end(e.clientX, e.clientY, e);
        }
    });
});
