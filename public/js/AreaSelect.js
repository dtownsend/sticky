import Draggable from './Draggable.js';
import Board from './Board.js';

export default class AreaSelect extends Draggable {
    constructor (x, y) {
        super(x, y, 0, 0, 'selection');
        this.type = 'AreaSelect';
        this.startX = x;
        this.startY = y;

        this.endX = 0;
        this.endY = 0;
        this.top = 0;
        this.right = 0;
        this.bottom = 0;
        this.left = 0;

        this.draggables = [];

        Board.selection = this;
    }

    getHtml() {
        return '';
    }

    end(x, y) {
        let self = this;

        if (!Board.selection) {
            return;
        }

        this.endX = x;
        this.endY = y;

        if (this.startY> this.endY) {
            this.top = this.endY;
            this.bottom = this.startY;
        } else {
            this.top = this.startY;
            this.bottom = this.endY;
        }

        if (this.startX > this.endX) {
            this.right = this.startX;
            this.left = this.endX;
        } else {
            this.right = this.endX;
            this.left = this.startX;
        }

        Board.draggables.forEach(function(draggable) {
            if (draggable === self) {
                return;
            }

            if ($(draggable.ele).offset().top >= self.top && $(draggable.ele).offset().top <= self.bottom) {
                if ($(draggable.ele).offset().left <= self.right && $(draggable.ele).offset().left >= self.left) {
                    self.draggables.push(draggable);
                }
            }
        });

        this.ele.offset({top: this.top, left: this.left});
        this.ele.width(this.right - this.left);
        this.ele.height(this.bottom - this.top);

        this.draggables.forEach(function(draggable) {
            draggable.ele.appendTo(self.ele);
            draggable.ele.css('top', draggable.ele.position().top - self.ele.position().top)
                .css('left', draggable.ele.position().left - self.ele.position().left);
        });
    }

    destroy() {
        let self = this;

        this.draggables.forEach(function(draggable) {
            draggable.ele.appendTo(Board.canvas);
            draggable.ele.css('top', self.ele.position().top + draggable.ele.position().top)
                .css('left', self.ele.position().left + draggable.ele.position().left);
            draggable.x = draggable.ele.position().left;
            draggable.y = draggable.ele.position().top;
        });

        super.destroy();

        Board.selection = null;
    }
}
