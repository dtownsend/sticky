import Connection from "./Connection.js";
import Sticky from "./Sticky.js";

export default new class Board {
    constructor() {
        this.id = null;
        this.public = false;
        this.draggables = [];
        this.connections = [];
        this.selection = null;
        this.jsonExport = null;
        this.longPressTimeout = null;
        this.canvas = $('#canvas');
        this.titleEle = $('input[name=title]');
        this.publicEle = $('input[name=public]');
        this.loadSelect = $('select[name=boards]');
        this.saveButton = $('button#save');
    }

    save() {
        let self = this;

        // Don't try to save if a board isn't loaded
        if (!self.titleEle) {
            return;
        }

        let toSave = {
            title: self.titleEle.val().length? self.titleEle.val(): 'Unnamed board',
            public: self.publicEle.prop('checked'),
            board: {
                draggables: [],
                connections: [],
            }
        };

        this.draggables.forEach(function(draggable) {
            if (draggable.type === 'AreaSelect') {
                // don't save selections
                return;
            }
            toSave.board.draggables.push(draggable.save());
        });

        this.connections.forEach(function(connection) {
            toSave.board.connections.push(connection.save());
        });

        // Don't try to save an empty board
        if (toSave.board.draggables.length === 0) {
            return;
        }

        let lastSave = this.jsonExport;
        this.jsonExport = JSON.stringify(toSave);

        // check there are changes to save
        if (lastSave === this.jsonExport) {
            return;
        }

        self.saveButton.text('Saving...');

        $.ajax({
            method: self.id? 'patch': 'post',
            url: 'api/board' + (self.id? '/' + self.id: ''),
            data: {'json': self.jsonExport}
        }).done(function(data) {
            $('#failed-to-save').hide();
            self.id = data.id;
            self.saveButton.text('Save');
        }).fail(function() {
            $('#failed-to-save').show();
        });

        // Update the list
        this.list();

        // Update the url
        let url = new URL(window.location.origin);
        url.searchParams.set('board', self.id);
        history.replaceState({}, toSave.title, url.href)
    };

    load(boardId = null) {
        let self = this;

        if (!boardId) {
            boardId = this.loadSelect.val();
        }

        $.ajax({
            method: 'get',
            url: 'api/board/' + boardId,
        }).done(function(data) {
            $('#failed-to-load').hide();

            // Destroy old board objects
            self.draggables.forEach(function (draggable) {
                draggable.destroy();
            });

            self.connections.forEach(function (draggable) {
                draggable.destroy();
            });

            if (self.selection) {
                self.selection.destroy();
            }

            // Cache some board data
            self.id = data.board.id;
            self.title = data.board.title;
            self.public = data.board.public;

            // Update the controls with this board's settings
            self.titleEle.val(self.title);
            self.publicEle.prop('checked', self.public);
            self.loadSelect.val(self.id);
            window.document.title = self.title + ' - Sticky';

            data.board.draggables.forEach(function(draggable) {
                // @TODO: any js could be inserted as the type when sending request
                let obj = eval(draggable.type);
                // ==== FIX THIS ===

                let draggableObj = new obj();
                draggableObj.serializable.forEach(function (item) {
                    draggableObj[item] = draggable[item];
                });
                draggableObj.load();
            });

            data.board.connections.forEach(function(connection) {
                let connectionObj = new Connection();

                connectionObj.serializable.forEach(function (item) {
                    connectionObj[item] = connection[item];
                });

                connectionObj.load();
            });
        }).fail(function(data) {
            $('#view-only').hide();
            $('#failed-to-load').show();
        });
    }

    list() {
        let self = this;

        $.ajax({
            method: 'get',
            url: 'api/board',
        }).done(function(data) {
            self.loadSelect.empty();
            self.loadSelect.append(new Option('', null));
            data.boards.forEach(function (board) {
                self.loadSelect.append(new Option(board.title, board.id));
            });
            // Select the current board
            self.loadSelect.val(self.id);
        });
    }
}
