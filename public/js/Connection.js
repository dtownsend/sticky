import Board from './Board.js';

export default class Connection {
    constructor (fromId, toId) {
        this.fromId = fromId;
        this.toId = toId;
        this.from = $('#' + this.fromId);
        this.to = $('#' + this.toId);
        this.id = ([1e7]+-1e3+-4e3+-8e3+-1e11).replace(/[018]/g, c =>
            (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
        );
        this.ele = null;
        this.serializable = [
            'id',
            'fromId',
            'toId'
        ];

        Board.connections.push(this);
    }

    save() {
        let self = this;
        let props = {};

        this.serializable.forEach(function (item) {
            props[item] = self[item];
        });

        return props;
    }

    load() {
        this.from = $('#' + this.fromId);
        this.to = $('#' + this.toId);

        this.putInDom();
    }

    destroy() {
        $(self.ele).connections('remove');

        Board.connections = Board.connections.filter(item => item !== this);
    }

    select() {
        let self = this;

        $(this.ele).addClass('active');

        if ($(this.ele).find('.connection-delete').length === 0) {
            let deleteBtn = $(this.ele).prepend('<button class="connection-delete">x</button>');

            deleteBtn.click(function(e) {
                $(self.ele).connections('remove');
            });
        }

        $(this.ele).find('.connection-delete').show();
    }

    putInDom() {
        this.from.connections({
            to: this.to,
            within: '#canvas',
            css: {border: '3px solid'},
            class: 'connection ' + this.id
        });

        this.ele = $('.' + this.id)[0];

        this.onEnteredDom();
    }

    onEnteredDom() {
        let self = this;

        // $('.connection').off('click');

        $(this.ele).click(function(e) {
            let clickedBorder = false;

            if (!e.offsetY) {
                e.offsetY = e.clientY - $(this)[0].offsetTop;
            }

            if (!e.offsetX) {
                e.offsetX = e.clientX - $(this)[0].offsetLeft;
            }

            if (e.offsetY > $(this).outerHeight() - 3){
                clickedBorder = true;
            }

            if (e.offsetY < 3){
                clickedBorder = true;
            }

            if (e.offsetX > $(this).outerWidth() - 3){
                clickedBorder = true;
            }

            if (e.offsetX < 3){
                clickedBorder = true;
            }

            if (!clickedBorder) {
                $(this).hide();

                let below = document.elementFromPoint(e.clientX, e.clientY);

                let event = new $.Event("click");

                event.clientX = e.clientX;
                event.clientY = e.clientY;

                $(below).trigger(event);

                $(this).show();

                return false;
            }

            self.select();

            e.preventDefault();
            return false;
        });
    }
}
