import Draggable from './Draggable.js';

export default class Sticky extends Draggable {
    constructor(x, y, width, height, text, colour) {
        super(x, y, width, height, 'sticky');

        this.text = text;
        this.colour = colour;
        this.fontSize = 21;
        this.type = 'Sticky';
        this.serializable = this.serializable.concat([
            'text',
            'colour',
            'fontSize',
        ]);
        this.editable = false;
    }

    save() {
        return super.save();
    }

    getHtml() {
        return '<div class="sticky" style="background-color:#'+this.colour+';font-size:'+this.fontSize+'px">'+
            this.text +
            '</div>'
    }

    onClicked(e) {
        if (this.selected && !this.editable) {
            this.ele.find('.sticky').attr('contenteditable', true);
            this.ele.addClass('editing');
            this.ele.draggable('disable');
            this.ele.find('.sticky').focus();

            document.execCommand('selectAll',false,null);

            this.editable = true;
        }
    }

    onEnteredDom() {
        super.onEnteredDom();

        let self = this;

        $(document).on('click', function (e) {

        });

        $(document).keyup(function (event) {
            if (!self.selected) {
                return;
            }

            self.text = self.ele.find('[contenteditable=true]').html();

            if (event.keycode === 'del') {
                return;
            }

            if (!self.text) {
                return;
            }

            // Set font to max and then reduce to fit
            self.setFontSize(21);

            let i = 0;

            while (event.target.scrollWidth > event.target.clientWidth){
                if (i > 20) {
                    break;
                }

                self.reduceFontSize();

                i++;
            }

            i = 0;

            while (event.target.scrollHeight > event.target.clientHeight){
                if (i > 20) {
                    break;
                }

                self.reduceFontSize();

                i++;
            }
        });
    }

    onExternalClick(e) {
        super.onExternalClick(e);

        if (this.editable) {
            this.ele.find('.sticky').attr('contenteditable', false);
            this.ele.removeClass('editing');
            this.ele.draggable('enable');

            this.editable = false;
            window.getSelection().empty();
        }
    }

    reduceFontSize() {
        let fontSize = this.ele.find('.sticky').css('font-size');
        fontSize = fontSize.substring(0, fontSize.indexOf('px'));

        this.setFontSize(fontSize - 1);
    }

    increaseFontSize() {
        let fontSize = this.ele.find('.sticky').css('font-size');
        fontSize = parseInt(fontSize.substring(0, fontSize.indexOf('px')));

        if (fontSize > 21) {
            return;
        }

        this.setFontSize(fontSize + 1);
    }

    setFontSize(fontSize)
    {
        this.ele.find('.sticky').css('font-size', fontSize);

        this.fontSize = fontSize;
    }
}
