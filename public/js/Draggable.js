import Board from './Board.js';
import Connection from './Connection.js';

export default class Draggable {
    constructor(x, y, width, height, cssClass) {
        this.id = null;
        this.ele = null;
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.type = 'Draggable';
        this.cssClass = 'draggable '+cssClass;
        this.serializable = [
            'id',
            'type',
            'x',
            'y',
            'width',
            'height',
            'class',
        ];
        this.selected = false;
        this.connecting = false;
        this.id = ([1e7]+-1e3+-4e3+-8e3+-1e11).replace(/[018]/g, c =>
            (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
        );

        Board.draggables.push(this);
    }

    save() {
        let self = this;
        let props = {};

        this.serializable.forEach(function (item) {
            props[item] = self[item];
        });

        return props;
    }

    load() {
        this.putInDom();
    }

    destroy() {
        this.ele.remove();

        Board.draggables = Board.draggables.filter(item => item !== this);
    }

    select() {
        this.ele.addClass('selected');
        this.selected = true;

        this.ele.find('.draggable-tool').show();
    }

    putInDom() {
        this.ele = $('<div id="'+this.id+'" class="'+this.cssClass+'" style="position:absolute;width:'+this.width+'px;height:'+this.height+'px;left:'+this.x+'px;top:'+this.y+'px">' +
            '<div class="draggable-tool" style="display:none">' +
            '<button class="draggable-tool-delete">x</button>' +
            '<button class="draggable-tool-connect"><-></button>' +
            '</div>' +
            this.getHtml()+
            '</div>'
        );

        Board.canvas.append(this.ele);

        this.onEnteredDom();
    }

    getHtml() {
        return 'Unknown entity';
    }

    onEnteredDom(){
        let self = this;

        this.ele.draggable({
            drag: function() {
                self.x = self.ele.position().left;
                self.y = self.ele.position().top;
            },
            grid: [20, 20],
            stop: function() {
                self.ele.connections('update');
            }
        });

        this.ele.on('click', function (e) {
            self.onClicked(e);

            if (!self.selected) {
                self.select();
            }
        });

        $(document).on('click', function (e) {
            if (e.target === self.ele[0]) {
                return;
            }

            if (e.target.parentElement === self.ele[0]) {
                return;
            }

            if ($(e.target).parents('.draggable')[0] === self.ele[0]) {
                return;
            }

            self.onExternalClick(e);
        });

        this.ele.find('.draggable-tool-delete').click(function (e) {
            self.destroy();
        });

        this.ele.find('.draggable-tool-connect').click(function (e) {
            self.connecting = true;
        });


    }

    onClicked(e){}

    onExternalClick(e){

        $('.connection-delete').hide();

        $('.connection').removeClass('active');

        if (this.connecting) {
            if ($(e.target).parents().hasClass('draggable')) {

                new Connection($(this.ele)[0].id, $(e.target).parents('.draggable')[0].id).putInDom();

                this.connecting = false;
            }
        }

        if (this.selected) {
            this.ele.removeClass('selected');
            this.selected = false;
        }

        this.ele.find('.draggable-tool').hide();
    }
}
